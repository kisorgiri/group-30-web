// web architecture
// MVC
// bidirectional flow of data  in controller and model
// component == > model view controller

// flux architecture
// views, actions, dispatcher,store
// unidirectional flow of data
// views ===> actions ===> dispatchers ===> store
// there can be multiple store
// state changing logic resides on store


// Redux(on top of flux)
// redux is independant state management tool
// views actions reducer store
// unidirectional data flow
// views ===> actions===> reducers ==> store
// there will be only one centralized store
// sore modification logic resides on reducer