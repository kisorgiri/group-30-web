// modern JS version

// Object shorthand
// Object Destruct
// Spread Operator
// Rest Operator
// Arrow notation function
// template literals
// default argument
// block scope(let)
// class
// import and export

// 1.Object shorthand
var color = 'black';

var laptop = {
    brand: 'dell',
    name: 'abc',
    generation: 'i7',
    color
}

// 2.Object Destruct

function askSomething() {
    var fruits = ['apple', 'grape'];
    var vegitables = ['potatto'];
    var fish = 'fish';
    var abc = 'xyz';

    return {
        fruits,
        vegitables,
        fish,
        abc
    }
}

var res = askSomething();
// console.log('res >>', res);

// const { fruits,abc } = res;
// console.log('fruits >>', fruits)
// console.log('abc >>', abc)

// const {vegitables:Kishor} = res;
// console.log('vegitables >>',Kishor)

// 3.spread Operator(...)
// mutation

// var obj = {
//     name: 'broadway',
//     addres: 'tinkune',
//     phone: 223
// }
// var next = {
//     email:'sdflsd'
// }
// console.log('obj >', obj)

// var obj2 = {
//     ...obj,
//     ...next,
//     status:'on',
//     name:'test'
// }
// // obj2.name ='infosys'
// console.log('ob2 ', obj2)

// rest operator
// var mobile = {
//     name: 'xyz',
//     brand: 'abc',
//     color: 'black',
//     price: 333
// }

// var xyz = {
//     ...mobile
// }

// var { name: Broadway, ...rest } = mobile;

// console.log('Broadway >>', Broadway);
// console.log('rest >>', rest)

// Arrow notation function

// function welcome(name,address){

// }

// const welcome = (name,address)=>{

// }
// const goodBye = name => {
//     // console.log('name is >>', name);
//     return 'hi ' + name;
// }
// console.log(goodBye('bibek'))

// const sum = (num1, num2) => num1 + num2;
// console.log('sum >>',sum(34,55))

var bikes = [
    {
        brand: 'honda',
        color: 'red'
    },
    {
        brand: 'yamaha',
        color: 'black'
    },
    {
        brand: 'suzuki',
        color: 'red'
    }
]

// const redBikes = bikes.filter(function (item) {
//     if (item.color === 'red') {
//         return true;
//     }
// })
// const redBikes = bikes.filter(bike => bike.color === 'red');
// console.log('red bikes >>', redBikes)


// main advantages of arrow notation function
// it will inherit parent this

function welcome(name, addr = 'nepal') {

    // var text = 'hi' + name + ' welcome tolkjsdlkjflask ' + addr;
    // console.log('text >>',text)
    // template literals
    var text = `hi ${name} and     welcome     to ${addr}`
    console.log('text >', text)
}

welcome('ritesh', 'bkt')

function calculate(abc = {}) {
    abc.xyz;

}
calculate();


// block scope
// var , let ===> for variable declaration
// var ==> if used inside a function no matter how many times we declare a variable with var it is single declaration
// let ==> if let is used inside function and inside differect block {} it maintains its scope

function sayHello(name) {
    var msg = 'hi';
    if (name) {
        var msg = 'hello';
    }
    else {
        var msg = 'how are you';
    }
}
function sayHello(name) {
    let msg = 'hi';
    if (name) {
        let msg = 'hello';
        // it maintains its scope within if block
    }
    else {
        let msg = 'how are you';
        // it maintains its scope within else block

    }
}


// import and export (file - file communication)

// export
// two ways of export in es6
// a.named export
// b. default export

// note there should be only one default export
// there can be multiple named export
// one file can have both named and default export

// syntax for named export
// export keyword with name
// eg export const address = 'value';
// export const name = '';

// var data  ={};
// default export 
// syntax
// export default data;

// import
// import totally depends on how it is exported
// if it is named export
// import syntax
// import { name, address } from './path_to_source';

// if it is default export
// import own_name from './path_to_soruce'

// if it is both named and default export
// import {a,b,c},data from './source'


// class===>
// class is is a group of fileds, methods and constructor

// Abstraction, Polymorphism, encapsulation, inheritance

// syntax
class Fruits {
    origin;
    constructor() {
        this.origin = 'nepal';
    }
    // private ==> within same class
    // protected ==> it will be accessible by child but not by instance
    // public ===> default ===> every properties and methods are accessible by child and instance

    getOrigin() {
        return this.origin;
    }

    setOrigin(newOrigin) {
        this.origin = newOrigin;
        // this.ge
        return this.origin;
    }

}
var apple = new Fruits(); // apple is instance

// inheritance
// parent == fruits
// child vegitables

class Vegitables extends Fruits {
    color;
    constructor() {
        this.color = 'green';
        super(); // super keyword is used to call parent class constructor
    }

    getColor() {
        // this.getOrigin()
        return this.color;
    }


}