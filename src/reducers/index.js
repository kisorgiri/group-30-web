import { combineReducers } from 'redux';
import { productInView } from './product.red';

export default combineReducers({
    product: productInView,
})