import { ADD_PRODUCTS, PRODUCTS_RECEIVED, PRODUCT_RECEIVED, PRODUCT_REMOVED, PRODUCT_UPDATED, SET_IS_LOADING, SET_PAGE_NUMBER } from "../actions/products/type"

// reducer is plain js function
const initialState = {
    isLoading: false,
    products: [],
    pageNumber: 1,
    pageSize: 5,
    product: {}
}

export const productInView = (state = initialState, action) => {
    console.log('at reducer', action);
    switch (action.type) {
        case SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }

        case PRODUCTS_RECEIVED:
            return {
                ...state,
                products: action.payload
            }

        case SET_PAGE_NUMBER:
            return {
                ...state,
                pageNumber: action.payload
            }
        case PRODUCT_REMOVED:
            const { products } = state;
            products.forEach((item, i) => {
                if (item._id === action.payload) {
                    products.splice(i, 1);
                }
            })
            return {
                ...state,
                products: [...products]
            }
        case ADD_PRODUCTS:
            return {
                ...state,
                products: action.payload
            }

        case PRODUCT_RECEIVED:
            return {
                ...state,
                product: action.payload
            }
        case PRODUCT_UPDATED:
            return {
                ...state,
                product: action.payload
            }



        default:
            return {
                ...state
            }
    }
}