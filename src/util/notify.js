import { toast } from 'react-toastify';

const showSuccess = (msg) => {
    return toast.success(msg);
}

const showInfo = (msg) => {
    return toast.info(msg);
}

const showWarnings = (msg) => {
    return toast.warning(msg);
}

const showError = (msg) => {
    toast.error(msg);
}

const handleError = (error) => {
    debugger;
    // error can be anything
    // parse error
    // extract error message
    // build errror message
    // show them through toast
    let errMsg = 'Something went wrong';
    let err = error && error.response;
    let serverError = err && err.data;
    if (serverError) {
        if (typeof (serverError.msg) === 'string') {
            errMsg = serverError.msg;
        }
    }

    showError(errMsg);

}

const notify = {
    showSuccess,
    showInfo,
    showWarnings,
    handleError
}
export default notify;