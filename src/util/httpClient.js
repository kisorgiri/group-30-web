import axios from 'axios';

const BASE_URL = process.env.REACT_APP_BASE_URL;

// create an instance
const http = axios.create({
    baseURL: BASE_URL,
    responseType: 'json'
});

const getHeaders = (isSecure) => {
    let options = {
        'Content-Type': 'application/json'
    }
    if (isSecure) {
        options['Authorization'] = localStorage.getItem('token')
    }

    return options;
}

const GET = (url, isSecure = false, params = {}) => {
    return http.get(url, {
        headers: getHeaders(isSecure),
        params
    });
}

const POST = (url, data, isSecure = false, params = {}) => {
    return http.post(url, data, {
        headers: getHeaders(isSecure),
        params
    });
}


const PUT = (url, data, isSecure = false, params = {}) => {
    return http.put(url, data, {
        headers: getHeaders(isSecure),
        params
    });
}

const DELETE = (url, isSecure = false, params = {}) => {
    return http.delete(url, {
        headers: getHeaders(isSecure),
        params
    });
}

const UPLOAD = (url, method, data, files = []) => {
    return new Promise((resolve, reject) => {
        // send XML HTTP REQUEST
        const xhr = new XMLHttpRequest();
        const formData = new FormData();

        // add files and textual data in formData
        if (files.length) {
            files.forEach(file => {
                formData.append('image', file, file.name)
            })
        }
        // add textual data in formData
        for (let key in data) {
            formData.append(key, data[key])
        }

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(xhr.response)
                } else {
                    reject(xhr.response)
                }
            }
        }

        // now intitiate request
        xhr.open(method, `${BASE_URL}/${url}?token=${localStorage.getItem('token')}`, true)
        xhr.send(formData);
    })

}

const methods = {
    GET,
    POST,
    PUT,
    DELETE,
    UPLOAD
}
export default methods;
