import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers';
import thunk from 'redux-thunk';

const middlewares = [thunk];


const initialState = {
    product: {
        isLoading: false,
        products: [],
        pageNumber: 1,
        pageSize: 5,
        product: {}
    },
    user: {
        isLoading: false,
        users: [],
        isSubmitting: false
    },
    notification: {

    },
    ratings: {

    }
}

export const store = createStore(rootReducer, initialState, applyMiddleware(...middlewares))