import React, { Component } from 'react'
import { Button } from '../../Common/Button/Button.component'
const defaultForm = {
    name: '',
    category: '',
    brand: '',
    price: '',
    size: '',
    color: '',
    description: '',
    warrentyStatus: '',
    warrentyPeroid: '',
    returnEligible: '',
    returnTimeFromPurchase: '',
    quantity: '',
    offers: '',
    tags: '',
    discountedItem: '',
    discountType: '',
    discountValue: ''
}

const IMG_URL = process.env.REACT_APP_IMG_URL;
export class ProductForm extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            filesToUpload: [],
            isValidForm: true
        }
    }

    componentDidMount() {
        // for edit
        const { productData } = this.props;
        if (productData) {
            this.setState({
                data: {
                    ...defaultForm,
                    ...productData,
                    discountedItem: productData.discount ? productData.discount.discountedItem : '',
                    discountType: productData.discount ? productData.discount.discountType : '',
                    discountValue: productData.discount ? productData.discount.discountValue : '',
                }
            })
        }
    }
    handleSubmit = e => {
        e.preventDefault();
        this.props.submitCallback(this.state.data, this.state.filesToUpload);
    }

    handleChange = e => {
        let { type, checked, name, value, files } = e.target;
        if (type === 'file') {
            const { filesToUpload } = this.state
            filesToUpload.push(files[0]);
            return this.setState({
                filesToUpload
            })
        }
        if (type === 'checkbox') {
            value = checked;
        }
        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })

    }

    validateForm = fieldName => {
        let errMsg = '';
        switch (fieldName) {
            case 'category':
            case 'name':
            case 'brand':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'required field*'

            default:
                break;
        }

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    render() {
        const { productData } = this.props;
        let discountContent = this.state.data.discountedItem
            ? <>
                <label>Discount Type</label>
                <select className="form-control" value={this.state.data.discountType} name="discountType" onChange={this.handleChange} >
                    <option value='' disabled>(Select Discount Type)</option>
                    <option value="percentage"> Percentage</option>
                    <option value="quantity">Quantity</option>
                    <option value="value">Value</option>

                </select>
                <label>Discount Value</label>
                <input type="text" className="form-control" value={this.state.data.discountValue} name="discountValue" placeholder="Discount Value" onChange={this.handleChange} />
            </>
            : ''
        return (
            <>
                <h2>{this.props.title}</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Name</label>
                    <input type="text" className="form-control" value={this.state.data.name} name="name" placeholder="Name" onChange={this.handleChange} />
                    <label>Description</label>
                    <textarea rows={5} className="form-control" value={this.state.data.description} name="description" placeholder="Description" onChange={this.handleChange} />
                    <label>Brand</label>
                    <input type="text" className="form-control" value={this.state.data.brand} name="brand" placeholder="Brand" onChange={this.handleChange} />
                    <label>Category</label>
                    <input type="text" className="form-control" value={this.state.data.category} name="category" placeholder="Category" onChange={this.handleChange} />
                    <label>Color</label>
                    <input type="text" className="form-control" value={this.state.data.color} name="color" placeholder="Color" onChange={this.handleChange} />
                    <label>Price</label>
                    <input type="number" className="form-control" value={this.state.data.price} name="price" placeholder="Price" onChange={this.handleChange} />
                    <label>Quantity</label>
                    <input type="number" className="form-control" value={this.state.data.quantity} name="quantity" placeholder="Quantity" onChange={this.handleChange} />
                    <input type="checkbox" checked={this.state.data.warrentyStatus} name="warrentyStatus" onChange={this.handleChange} />
                    <label>Warrenty Status</label>
                    <br />
                    {
                        this.state.data.warrentyStatus && (
                            <>
                                <label>Warrenty Peroid</label>
                                <input type="text" className="form-control" value={this.state.data.warrentyPeroid} name="warrentyPeroid" placeholder="Warrenty Peroid" onChange={this.handleChange} />
                            </>
                        )
                    }
                    <label>Tags</label>
                    <input type="text" className="form-control" value={this.state.data.tags} name="tags" placeholder="Tags" onChange={this.handleChange} />
                    <label>Offers</label>
                    <input type="text" className="form-control" value={this.state.data.offers} name="offers" placeholder="Offers" onChange={this.handleChange} />
                    <input type="checkbox" name="discountedItem" checked={this.state.data.discountedItem} onChange={this.handleChange} />
                    <label>Discounted Item</label>
                    <br />
                    {discountContent}
                    <input type="checkbox" name="returnEligible" checked={this.state.data.returnEligible} onChange={this.handleChange} />
                    <label>Return Eligible</label>
                    <br />
                    {
                        this.state.data.returnEligible && (
                            <>
                                <label>Return Time In Day</label>
                                <input type="number" className="form-control" value={this.state.data.returnTimeFromPurchase} name="returnTimeFromPurchase" onChange={this.handleChange} />

                            </>
                        )
                    }
                    {
                        productData && productData.images && productData.images.length && (
                            <>
                                <label>Previous Image</label>
                                <br />
                                <img src={`${IMG_URL}/${productData.images[0]}`} alt="image.png" width="400px"></img>
                                <br />
                            </>
                        )
                    }
                    <label>Choose Images</label>
                    <input className="form-control" type="file" onChange={this.handleChange} />

                    <br />
                    <Button
                        isSubmitting={this.props.isSubmitting}
                        isValidForm={this.state.isValidForm}>

                    </Button>
                </form>

            </>
        )
    }
}
