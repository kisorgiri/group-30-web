import React, { Component } from 'react'
import httpClient from './../../../util/httpClient';
import notify from '../../../util/notify';
import { ProductForm } from './../ProductForm/ProductForm.component';
import { Loader } from '../../Common/Loader/Loader.component';
export class EditProduct extends Component {
    constructor() {
        super()

        this.state = {
            isLoading: false,
            isSubmitting: false,
            product: {}
        }
    }
    componentDidMount() {
        this.setState({
            isLoading: true
        })
        this.productId = this.props.match.params['id'];
        httpClient.GET(`/product/${this.productId}`, true)
            .then(response => {
                this.setState({
                    product: response.data
                })
            })
            .catch(err => {
                notify.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }

    edit = (data, files) => {
        this.setState({
            isSubmitting: true
        })
        // api call
        httpClient.UPLOAD(`product/${this.productId}`, 'PUT', data, files)
            .then(response => {
                notify.showInfo('Proudct Updated')
                this.props.history.push('/view_product')
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        let content = this.state.isLoading
            ? <Loader />
            : <ProductForm
                isSubmitting={this.state.isSubmitting}
                submitCallback={this.edit}
                productData={this.state.product}
            ></ProductForm>
        return content;
    }
}
