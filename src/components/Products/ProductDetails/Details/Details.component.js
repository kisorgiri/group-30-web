import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchById_ac } from './../../../../actions/products/product.ac';

class DetailsComponent extends Component {
    componentDidUpdate(prevProps) {
        if (prevProps.productId !== this.props.productId) {
            this.props.fetchProduct(this.props.productId)
        }
    }
    render() {
        const { product } = this.props
        return (
            <>
                <h2>Product Details</h2>
                <p>Category:{product.category}</p>
                <p>Name:{product.name}</p>
                <p>Brand:{product.brand}</p>
                <p>Price:{product.price}</p>
                <p>Color:{product.color}</p>
                <p>Tags:{product.tags}</p>
                <p>show every product propety appropriately</p>
                <p>Show Image slider to show multiple images</p>
                {(product.ratings || []).map((rating, index) => (
                    <div key={index}>
                        <p>{rating.point}</p>
                        <p>{rating.message}</p>
                        <p>{rating.createdAt}</p>
                        <p>{rating.user}</p>
                    </div>
                ))}
            </>
        )
    }
}

const mapStateToProps = rootStore => ({
    product: rootStore.product.product
})

const mapDispatchToProps = dispatch => ({
    fetchProduct: (id) => { dispatch(fetchById_ac(id)) }
})

export const Details = connect(mapStateToProps, mapDispatchToProps)(DetailsComponent)
