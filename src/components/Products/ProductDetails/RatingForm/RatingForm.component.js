import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addRatings_ac } from '../../../../actions/products/product.ac'
import { Button } from '../../../Common/Button/Button.component'

class RatingFormComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            ratingMsg: '',
            ratingPoint: ''
        }
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }
    handleSubmit = e => {
        const { ratingMsg, ratingPoint } = this.state;
        e.preventDefault();
        this.props.add_ratings(this.props.productId, { ratingMsg, ratingPoint })
    }

    render() {
        return (
            <>
                <h2>Add Ratings</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Point</label>
                    <input type="number" name="ratingPoint" className="form-control" onChange={this.handleChange}></input>
                    <label>Message</label>
                    <input type="text" name="ratingMsg" className="form-control" onChange={this.handleChange}></input>
                    <br />
                    <Button
                        isSubmitting={false}
                        isValidForm={true}
                    ></Button>
                </form>
            </>
        )
    }
}
const mapStateToProps = rootStore => {

}
const mapDispatchToProps = dispatch => ({
    add_ratings: (id, ratings) => { dispatch(addRatings_ac(id, ratings)) }
})

export const RatingForm = connect(mapStateToProps, mapDispatchToProps)(RatingFormComponent)
