import React, { Component } from 'react'
import { Details } from './Details/Details.component';
import { RatingForm } from './RatingForm/RatingForm.component';
export class ProductDetails extends Component {
    constructor(props) {
        super(props)

        this.state = {
            productId: ''
        }
    }

    componentDidMount() {
        this.productId = this.props.match.params['id'];
        this.setState({
            productId: this.productId
        })

    }
    render() {
        return (
            <>
                <Details productId={this.state.productId} />
                <RatingForm productId={this.state.productId} />
            </>
        )
    }
}
