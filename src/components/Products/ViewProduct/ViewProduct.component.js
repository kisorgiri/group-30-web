import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { formatDate, formatTime } from './../../../util/dateUtil';
import { Loader } from '../../Common/Loader/Loader.component';
import { connect } from 'react-redux'
import { fetch_product_ac, changePageNumber_ac, removeProduct_ac, addProductInStore_ac } from './../../../actions/products/product.ac';
const IMG_URL = process.env.REACT_APP_IMG_URL;

class ViewProductComponent extends Component {
    componentDidMount() {
        console.log('check props >>', this.props)
        if (this.props.results) {
            this.props.addProductInStore(this.props.results);

        } else {
            this.props.fetch(this.props.pageNumber, this.props.pageSize);
        }
    }

    edit = id => {
        this.props.history.push(`edit_product/${id}`);
    }

    remove = (id, i) => {
        // ask confirmation
        // use modal(dialog) for confirmation
        // go to awesome react component to find appropriate modal
        const confirmation = window.confirm("Are you sure to remove?");
        if (confirmation) {
            this.props.removeProduct(id)
        }
    }

    changePage = (val) => {
        // TODO performRedux cycle
        let currentPage = this.props.pageNumber;
        if (val === 'next') {
            currentPage += 1;
        }
        if (val === 'previous') {
            currentPage -= 1;

        }
        this.props.upatePageNumber(currentPage);
        this.props.fetch(currentPage, this.props.pageSize);
    }

    render() {
        let content = this.props.isLoading
            ? <Loader></Loader>
            : <>
                <h2>View Products</h2>
                {
                    this.props.results && (
                        <button className="btn btn-success" onClick={this.props.searchAgain}>Search Again</button>
                    )
                }
                <table className="table">
                    <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Price</th>
                            <th>Created At</th>
                            <th>Tags</th>
                            <th>Images</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.products.map((product, i) => (
                            <tr key={product._id}>
                                <td>{i + 1}</td>
                                <td> <Link to={`/product_details/${product._id}`}>{product.name}</Link></td>
                                <td>{product.category}</td>
                                <td>{product.brand}</td>
                                <td>{product.price}</td>
                                <td> {formatDate(product.createdAt)}</td>
                                <td>{(product.tags || []).join(',')}</td>
                                <td>
                                    <img src={`${IMG_URL}/${product.images[0]}`} alt="product.png" width="200px"></img>
                                </td>
                                <td>
                                    <button onClick={() => this.edit(product._id)} className="btn btn-info">edit</button>
                                    <button onClick={() => this.remove(product._id, i)} className="btn btn-danger">delete</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                {
                    this.props.pageNumber > 1 && (
                        <button onClick={() => this.changePage('previous')} className="btn btn-success">Previous</button>
                    )
                }
                <button onClick={() => this.changePage('next')} className="btn btn-success">Next</button>
            </>
        return content;
    }
}

// map state to props
// incoming value for component inside props
const mapStateToProps = (rootStore) => ({
    products: rootStore.product.products,
    isLoading: rootStore.product.isLoading,
    pageNumber: rootStore.product.pageNumber,
    pageSize: rootStore.product.pageSize
})

const mapDispatchToProps = dispatch => ({
    fetch: (pageNumber, pageSize) => { dispatch(fetch_product_ac({ pageNumber, pageSize })) },
    upatePageNumber: (pageNumber) => { dispatch(changePageNumber_ac(pageNumber)) },
    removeProduct: (id) => { dispatch(removeProduct_ac(id)) },
    addProductInStore: (products) => { dispatch(addProductInStore_ac(products)) }

})


export const ViewProduct = connect(mapStateToProps, mapDispatchToProps)(ViewProductComponent)