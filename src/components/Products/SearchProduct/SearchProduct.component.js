import React, { Component } from 'react'
import { Button } from './../../Common/Button/Button.component'
import httpClient from './../../../util/httpClient';
import notify from './../../../util/notify'
import { ViewProduct } from './../ViewProduct/ViewProduct.component'

const defaultForm = {
    name: '',
    category: '',
    minPrice: '',
    maxPrice: '',
    brand: '',
    fromDate: '',
    toDate: '',
    tags: '',
    offers: '',
    multipleDateRange: ''
}
export class SearchProduct extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            allProudcts: [],
            categories: [],
            names: [],
            isSubmitting: false,
            isValidForm: false,
            searchResults: []
        }
    }

    componentDidMount() {
        httpClient.POST('/product/search', {})
            .then(response => {
                let categories = [];
                response.data.forEach((item, index) => {
                    if (!categories.includes(item.category)) {
                        categories.push(item.category)
                    }
                })
                this.setState({
                    allProudcts: response.data,
                    categories
                })
            })
            .catch(err => {
                notify.handleError(err);
            })
    }
    handleChange = e => {
        let { type, name, value, checked } = e.target;
        if (name === 'category') {
            this.prepareNames(value);
        }
        if (type === 'checkbox') {
            value = checked
        }

        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    prepareNames = (categoryName) => {

        let names = this.state.allProudcts.filter(product => product.category === categoryName);
        this.setState({
            names: names
        })
    }

    validateForm = fieldName => {
        let errMsg;
        switch (fieldName) {

            case 'name':
            case 'category':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'required field*'
                break;

            default:
                break;
        }

        this.setState(pre => ({
            erorr: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        const { data } = this.state;
        if (!data.multipleDateRange) {
            data.toDate = data.fromDate;
        }
        httpClient.POST('/product/search', data)
            .then(response => {
                if (!response.data.length) {
                    notify.showInfo("No any Product matched your search query");
                }
                this.setState({
                    searchResults: response.data
                })
            })
            .catch(err => {
                notify.handleError(err);
            })
    }

    searchAgain = () => {
        this.setState({
            searchResults: [],
            data: {
                ...defaultForm
            }
        })
    }

    render() {
        let content = this.state.searchResults.length > 0
            ? <>
                <ViewProduct results={this.state.searchResults} searchAgain={this.searchAgain} />
            </>
            : <>
                <h2>Search Product</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Category</label>
                    <select name="category" className="form-control" value={this.state.data.category} onChange={this.handleChange} >
                        <option disabled value="">(Select Category)</option>
                        {
                            this.state.categories.map((cat, index) => (
                                <option key={index} value={cat}>{cat}</option>
                            ))
                        }
                    </select>
                    {
                        this.state.names.length > 0 && (
                            <>
                                <label>Name</label>
                                <select className="form-control" name="name" value={this.state.data.name} onChange={this.handleChange} >
                                    <option value="">(Select Name)</option>
                                    {this.state.names.map((item, i) => (
                                        <option key={i} value={item.name}>{item.name}</option>
                                    ))}
                                </select>
                            </>
                        )
                    }

                    <label>Brand</label>
                    <input type="text" className="form-control" name="brand" placeholder="Brand" onChange={this.handleChange} />
                    <label>Min Price</label>
                    <input type="number" className="form-control" name="minPrice" onChange={this.handleChange} />
                    <label>Max Price</label>
                    <input type="number" className="form-control" name="maxPrice" onChange={this.handleChange} />
                    <label>Select Date</label>
                    <input type="date" className="form-control" name="fromDate" onChange={this.handleChange} />
                    <input type="checkbox" name="multipleDateRange" onChange={this.handleChange}></input>
                    <label>Multiple Date Range</label>
                    <br />
                    {
                        this.state.data.multipleDateRange && (
                            <>
                                <label>To Date</label>
                                <input type="date" className="form-control" name="toDate" onChange={this.handleChange} />
                            </>
                        )
                    }

                    <label>Tags</label>
                    <input type="text" className="form-control" name="tags" placeholder="Tags" onChange={this.handleChange} />
                    <label>Offers</label>
                    <input type="text" className="form-control" name="offers" placeholder="Offers" onChange={this.handleChange} />
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                    ></Button>
                </form>
            </>

        return content;

    }
}

