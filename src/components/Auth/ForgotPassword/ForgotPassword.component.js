import React, { Component } from 'react'
import { Button } from './../../Common/Button/Button.component'
import httpClient from './../../../util/httpClient';
import notify from './../../../util/notify';

export class ForgotPassword extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                email: ''
            },
            error: {
                email: ''
            },
            isValidForm: false,
            isSubmitting: false
        }
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState({
            data: {
                [name]: value
            }
        }, () => {
            this.validateForm(name);
        })
    }

    validateForm = fieldName => {
        let errMsg = this.state.data[fieldName]
            ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                ? ''
                : 'invalid email'
            : 'required field*'

        this.setState({
            error: {
                [fieldName]: errMsg
            },
            isValidForm: !errMsg
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        httpClient.POST('/auth/forgot-password', this.state.data)
            .then(response => {
                notify.showInfo("Password Reset Link sent to your email please check your inbbox");
                this.props.history.push('/');
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })

    }

    render() {
        return (
            <>
                <h2>Forgot Password</h2>
                <p>Please Provide your email address to reset your password</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Email</label>
                    <input type="text" className="form-control" placeholder="Email Address" name="email" onChange={this.handleChange} />
                    <p className="error">{this.state.error.email}</p>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                    >

                    </Button>
                </form>
            </>
        )
    }
}
