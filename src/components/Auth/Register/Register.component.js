import React, { Component } from 'react';
import { Button } from '../../Common/Button/Button.component';
import { Link } from 'react-router-dom';
import notify from './../../../util/notify'
import httpClient from './../../../util/httpClient';

const defaultForm = {
    name: '',
    email: '',
    username: '',
    password: '',
    confirmPassword: '',
    phoneNumber: '',
    dob: '',
    tempAddress: '',
    permanentAddress: '',
    gender: ''
}
export class Register extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm: false
        };
    }
    handleChange = e => {
        const { name, value } = e.target;
        this.setState((preState) => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm = (fieldName) => {
        let errMsg;

        switch (fieldName) {
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].length > 6
                        ? ''
                        : 'username must be 6 characters long'
                    : 'requied field*'
                break;
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data.confirmPassword
                        ? this.state.data.confirmPassword === this.state.data[fieldName]
                            ? ''
                            : 'Password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'weak password'
                    : 'required field*'
                break;
            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ? ''
                        : 'Invalid Email'
                    : 'required field*'
                break;
            case 'confirmPassword':
                errMsg = this.state.data[fieldName]
                    ? this.state.data.password
                        ? this.state.data.password === this.state.data[fieldName]
                            ? ''
                            : 'Password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'Weak Password'
                    : 'required field*'
                break;

            default:
                break;
        }

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        this.setState({
            isSubmitting: true
        })
        e.preventDefault();
        // API call
        httpClient.POST('/auth/register',this.state.data)
            .then(response => {
                notify.showSuccess('Registration successfull, please login to start');
                this.props.history.push('/');
            })
            .catch(err => {
                this.setState({
                    isSubmitting: false
                });
                notify.handleError(err);
            })
    }

    render() {
        return (
            <div>
                <h2>Register</h2>
                <p>Please Register to Continue</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Name</label>
                    <input className="form-control" type="text" placeholder="Name" name="name" onChange={this.handleChange} />
                    <label>Phone Number</label>
                    <input className="form-control" type="number" name="phoneNumber" onChange={this.handleChange} />
                    <label>Username</label>
                    <input className="form-control" type="text" placeholder="Username" name="username" onChange={this.handleChange} />
                    <p className="error">{this.state.error.username}</p>
                    <label>Password</label>
                    <input className="form-control" type="password" placeholder="Password" name="password" onChange={this.handleChange} />
                    <p className="error">{this.state.error.password}</p>
                    <label>Confirm Password</label>
                    <input className="form-control" type="password" placeholder="Confirm Password" name="confirmPassword" onChange={this.handleChange} />
                    <p className="error">{this.state.error.confirmPassword}</p>

                    <label>Email</label>
                    <input className="form-control" type="text" placeholder="Email" name="email" onChange={this.handleChange} />
                    <p className="error">{this.state.error.email}</p>

                    <label>Gender</label>
                    <input className="form-control" type="text" placeholder="Gender" name="gender" onChange={this.handleChange} />
                    <label>Date of Birth</label>
                    <input className="form-control" type="date" name="dob" onChange={this.handleChange} />
                    <label>Temporary Address</label>
                    <input className="form-control" type="text" placeholder="Temporary Address" name="tempAddress" onChange={this.handleChange} />
                    <label>Permanent Address</label>
                    <input className="form-control" type="text" placeholder="Permanent Address" name="permanentAddress" onChange={this.handleChange} />
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                    ></Button>
                </form>
                <p>Already Registered?</p>
                <p><Link to="/">back to login</Link></p>
            </div>
        )
    }
}