import React, { Component } from 'react'
import { Button } from './../../Common/Button/Button.component'
import httpClient from './../../../util/httpClient';
import notify from './../../../util/notify';

export class ResetPassword extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                password: '',
                confirmPassword: ''
            },
            error: {
                password: '',
                confirmPassword: ''
            },
            isValidForm: false,
            isSubmitting: false
        }
    }

    componentDidMount() {
        this.token = this.props.match.params['token'];
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState((preState) => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm = (fieldName) => {
        let errMsg;

        switch (fieldName) {

            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data.confirmPassword
                        ? this.state.data.confirmPassword === this.state.data[fieldName]
                            ? ''
                            : 'Password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'weak password'
                    : 'required field*'
                break;

            case 'confirmPassword':
                errMsg = this.state.data[fieldName]
                    ? this.state.data.password
                        ? this.state.data.password === this.state.data[fieldName]
                            ? ''
                            : 'Password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'Weak Password'
                    : 'required field*'
                break;

            default:
                break;
        }

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        httpClient.POST(`/auth/reset-password/${this.token}`, this.state.data)
            .then(response => {
                notify.showInfo("Password reset successful please login");
                this.props.history.push('/');
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })

    }

    render() {
        return (
            <>
                <h2>Reset Password</h2>
                <p>Please choose your password wisely</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Password</label>
                    <input className="form-control" type="password" placeholder="Password" name="password" onChange={this.handleChange} />
                    <p className="error">{this.state.error.password}</p>
                    <label>Confirm Password</label>
                    <input className="form-control" type="password" placeholder="Confirm Password" name="confirmPassword" onChange={this.handleChange} />
                    <p className="error">{this.state.error.confirmPassword}</p>

                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                    >

                    </Button>
                </form>
            </>
        )
    }
}
