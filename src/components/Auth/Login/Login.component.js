import React, { Component } from 'react';
import { Button } from '../../Common/Button/Button.component';
import { Link } from 'react-router-dom';
import notify from './../../../util/notify';
import httpClient from './../../../util/httpClient'

const defaultForm = {
    username: '',
    password: ''
}
export class Login extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            remember_me: false,
            isSubmitting: false,
            isValidForm: false,
        };
    }

    componentDidMount() {
        var remember_me = localStorage.getItem('remember_me');

        if (remember_me === 'true') {
            this.props.history.push('/dashboard/sdlf');
        }
    }

    handleChange = (e) => {
        const { type, name, value, checked } = e.target;
        if (type === 'checkbox') {
            return this.setState({
                remember_me: checked
            })
        }

        this.setState(pre => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
        // form validation
    }
    validateForm = (fieldName) => {
        let errMsg = this.state.data[fieldName]
            ? ''
            : 'required field*'

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            });
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        // API call
        httpClient.POST('/auth/login', this.state.data)
            .then(response => {
                console.log('resposne >>', response);
                notify.showSuccess(`Welcome ${response.data.user.username}`);
                // localstorage
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('user', JSON.stringify(response.data.user));
                localStorage.setItem('remember_me', this.state.remember_me);
                this.props.history.push('/dashboard');
                // localStorage

            })
            .catch(err => {
                this.setState({
                    isSubmitting: false
                })
                notify.handleError(err);
            })
    }

    render() {

        return (
            <div>
                <h2>Login</h2>
                <p>Please Login to start your session</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label htmlFor="username">Username</label>
                    <input className="form-control" type="text" placeholder="Username" name="username" id="username" onChange={this.handleChange} />
                    <p className="error">{this.state.error.username}</p>
                    <label htmlFor="password">Password</label>
                    <input className="form-control" type="password" name="password" placeholder="Password" id="password" onChange={this.handleChange} />
                    <p className="error">{this.state.error.password}</p>
                    <input type="checkbox" name="remember_me" onChange={this.handleChange} />
                    <label>Rememebr Me</label>

                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                        disabledLabel="Loginingin..."
                        enabledLabel="Login"
                    >

                    </Button>
                </form>
                <p>Don't have an account?</p>
                <p> Register <Link to="/register">here</Link></p>
                <p><Link to="/forgot_password">forgot password?</Link></p>

            </div>
        )
    }
}

// life cycle
// INIT ==> 
// UPDATE ==>
// DESTROY ==>