import React, { Component } from 'react'
import * as io from 'socket.io-client';

export class Chat extends Component {
    constructor() {
        super()

        this.state = {

        }
    }
    componentDidMount() {
        this.socket = io(process.env.REACT_APP_SOCKET_URL);
        this.runSocket();
    }
    runSocket() {
        this.socket.on('welcome', (data) => {
            console.log('message from server >>', data);
            this.socket.emit('hello', 'hi from client')
        })
    }

    render() {
        return (
            <div>
                <p>Lets Chat</p>
            </div>
        )
    }
}
