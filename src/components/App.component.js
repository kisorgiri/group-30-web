import React, { Component } from 'react';
import { AppRouting } from './App.routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Provider } from 'react-redux';
import { store } from './../store';

export const App = (arg) => {
    // arg is incoming data for App so arg is props
    return (
        <div>
            <Provider store={store}>
                <AppRouting />
            </Provider>
            <ToastContainer />
        </div>
    )
}


// revision
// Component
// stateless vs statefull
// class based vs functional

// class based component
// inheritance with component of react
// constructor with super 
// state and props are attached with this
// render method is mandatory in class
// render method always returns single html node
// UI logic must be kept inside render

// functional component
// function must return a single html node
// functional component are stateless component (below react 16.8)




// component ==> component is basic building block for react application
// react is a library whose main purpose is to create a reusuabe UI component
// component is responsible for defining a html node
// component will always returns a single html node

// component can be stateful and stateless
// component can be class based or functional

// if a component have to maintain data within itself we need statefull component

// class based component ===> stateful
// -- props
// if we have to maintain data witin component

// functional component ===> stateless
// ---props
// Button, NavBar,App


// glossary 
// props ==> incoming data for a componenet
// state ==> application data that needs maintained within a component



