import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import React, { Component } from 'react';
import { Login } from './Auth/Login/Login.component';
import { Register } from './Auth/Register/Register.component';
import { NavBar } from './Common/NavBar/NavBar.component';
import { SideBar } from './Common/SideBar/Sidebar.component'
import { AddProduct } from './Products/AddProduct/AddProduct.component';
import { ViewProduct } from './Products/ViewProduct/ViewProduct.component';
import { EditProduct } from './Products/EditProduct/EditProduct.component';
import { SearchProduct } from './Products/SearchProduct/SearchProduct.component';
import { ForgotPassword } from './Auth/ForgotPassword/ForgotPassword.component';
import { ResetPassword } from './Auth/ResetPassword/ResetPassword.component';
import { Chat } from './Users/Messages/Messages.component';
import { ProductDetails } from './Products/ProductDetails/Index';

const Home = (props) => {
    return <p>Prepare Home Page</p>
}

const Dashboard = (props) => {
    return <p>Welcome to Group30 Store Please use side navigation menu or contact system administrator for support</p>
}

const NotFound = (props) => {
    return (<div>
        <p>Not Found</p>
        <img src="./images/download.jpeg" alt="image.png"></img>
    </div>)
}

const ProtectedRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={routeProps => (
            localStorage.getItem('token')
                ? <>
                    <div>
                        <NavBar isLoggedIn={true}></NavBar>
                    </div>
                    <div>
                        <SideBar isLoggedIn={true} />
                    </div>
                    <div className="main">
                        <Component {...routeProps} />
                    </div>
                </>
                : <Redirect to="/"></Redirect>
        )}></Route>
    )

}
const PublicRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={routeProps => (
            <>
                <div>
                    <NavBar isLoggedIn={localStorage.getItem('token') ? true : false}></NavBar>
                </div>
                <div>
                    <SideBar isLoggedIn={localStorage.getItem('token') ? true : false}></SideBar>
                </div>
                <div className="main">
                    <Component {...routeProps} />
                </div>
            </>
        )}></Route>
    )

}

export const AppRouting = (props) => {
    return (
        <BrowserRouter>
            <Switch>
                <PublicRoute exact path="/" component={Login}></PublicRoute>
                <PublicRoute path="/register" component={Register}></PublicRoute>
                <PublicRoute path="/home" component={Home}></PublicRoute>
                <ProtectedRoute path="/add_product" component={AddProduct}></ProtectedRoute>
                <ProtectedRoute path="/view_product" component={ViewProduct}></ProtectedRoute>
                <ProtectedRoute path="/edit_product/:id" component={EditProduct}></ProtectedRoute>
                <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
                <ProtectedRoute path="/chat" component={Chat}></ProtectedRoute>
                <ProtectedRoute path="/product_details/:id" component={ProductDetails}></ProtectedRoute>
                <PublicRoute path="/search_product" component={SearchProduct}></PublicRoute>
                <PublicRoute path="/forgot_password" component={ForgotPassword} ></PublicRoute>
                <PublicRoute path="/reset_password/:token" component={ResetPassword} ></PublicRoute>
                <PublicRoute component={NotFound}></PublicRoute>
            </Switch>

        </BrowserRouter>
    )
}

// react-router-dom ==> library for routing
// BrowserRouter == wrapper for overal routing
// Route ==> configuration for path and component
// flag => exact
// Switch==>( to handle 404)
// Link ==> it is used to navigate form user's mouse click event
// history,match,location are provided as props whene component are registered inside Route
// history ==> it is mostly used for navigation
// location==> optional search string, data in routing
// match==> dynamic url ko value consumption
