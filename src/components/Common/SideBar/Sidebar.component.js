import React from 'react';
import './Sidebar.component.css'
import { Link } from 'react-router-dom'

export const SideBar = props => {
    let content = props.isLoggedIn
        ? <div className="sidenav">
            <Link to="/dashboard">Home</Link>
            <Link to="/add_product">Add Product</Link>
            <Link to="/view_product">View Product</Link>
            <Link to="/search_product">Search Product</Link>
            <hr></hr>
            <Link to="/profile">Profile</Link>
            <hr></hr>
            <Link to="/chat">Messages</Link>
        </div>
        : ''
    return content
}