// functinoal component
import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import './NavBar.component.css'

const logout = (history) => {
    // 1. clear localstorage
    // 2. navigate to login
    localStorage.clear();
    history.push('/');
}
const NavBarComponent = (props) => {
    let content = props.isLoggedIn
        ? <ul className="nav_list">
            <li className="nav_item">
                <Link to="/dashboard">Home</Link>
            </li>        
      
            <li className="logout">
                <button className="btn btn-success" onClick={() => logout(props.history)} >logout</button>
            </li>
        </ul>
        : <ul className="nav_list">
            <li className="nav_item">
                <Link to="/home">Home</Link>
            </li>
            <li className="nav_item">
                <Link to="/">Login</Link>
            </li>
            <li className="nav_item">
                <Link to="/register">Register</Link>

            </li>
        </ul>
    return (
        <div className="nav_bar">
            {content}
        </div>
    )
}

export const NavBar = withRouter(NavBarComponent);