// action file where function are kept and these function are triggered from views
import httpClient from './../../util/httpClient'
import notify from './../../util/notify'
import { ADD_PRODUCTS, PRODUCT_UPDATED, PRODUCTS_RECEIVED, PRODUCT_RECEIVED, PRODUCT_REMOVED, SET_IS_LOADING, SET_PAGE_NUMBER } from "./type"

// function fetchProduct_ac(params) {
//     return function (dispatch) {

//     }
// }

export const fetch_product_ac = (params) => (dispatch) => {
    console.log('params is >>', params);
    console.log('inside action file')
    dispatch(isLoading(true))
    httpClient.GET('product', true, params)
        .then(response => {
            dispatch({
                type: PRODUCTS_RECEIVED,
                payload: response.data
            })
        })
        .catch(err => {
            notify.handleError(err);
        })
        .finally(() => {
            dispatch(isLoading(false))
        })
}


export const changePageNumber_ac = pageNumber => dispatch => {
    dispatch({
        type: SET_PAGE_NUMBER,
        payload: pageNumber
    })
}

/**
 * remove product
 * @param {String} id 
 * @param {Number} i 
 */
export const removeProduct_ac = id => (dispatch) => {
    httpClient.DELETE(`/product/${id}`, true)
        .then(response => {
            notify.showInfo("Product Removed")
            dispatch({
                type: PRODUCT_REMOVED,
                payload: id
            })
        })
        .catch(err => {
            notify.handleError(err);
        })
}

export const addProductInStore_ac = (products) => dispatch => {

    dispatch({
        type: ADD_PRODUCTS,
        payload: products
    })
}

export const fetchById_ac = (id) => dispatch => {
    httpClient.GET(`product/${id}`, true)
        .then(response => {
            dispatch({
                type: PRODUCT_RECEIVED,
                payload: response.data
            })
        })
        .catch(err => {
            notify.handleError(err);
        })
        .finally(() => {
            dispatch(isLoading(false))
        })
}

export const addRatings_ac = (id, data) => dispatch => {

    httpClient.PUT(`product/${id}`, data, true)
        .then(response => {
            notify.showInfo('Ratings Added')
            dispatch({
                type: PRODUCT_UPDATED,
                payload: response.data
            })
        })
        .catch(err => {
            notify.handleError(err);

        })
}

const isLoading = loading => ({
    type: SET_IS_LOADING,
    payload: loading
})